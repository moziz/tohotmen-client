# Töhötmen personal mobile client #
Personal mobile client (Unity based) for Murder of Gillian Groover. Used for all player actions, like swapping items with others, and showing your personal status.

https://globalgamejam.org/2018/games/murder-gillian-groover-t%C3%B6h%C3%B6-hitman

You will also need the server software to be able to play the game! The server source code is available at https://bitbucket.org/moziz/tohotmen-server
