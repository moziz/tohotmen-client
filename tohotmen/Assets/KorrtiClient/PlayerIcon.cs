﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerIcon : MonoBehaviour
{

    public Image icon;
    public Text nameLabel;

    public void Init(Sprite playerIcon, string playerName)
    {
        this.icon.sprite = playerIcon;
        this.nameLabel.text = playerName;
    }
}
