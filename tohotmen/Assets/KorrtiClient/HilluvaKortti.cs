﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HilluvaKortti : MonoBehaviour
{

    public delegate void ValmisEventti();
    public event ValmisEventti Valmis;

    public float baseScale = 1;
    public float scaleAmount = 0.1f;
    public float scaleSpeed = 2;
    public bool cycleScale = false; 

    private float speed = 0.3f;
    private Vector3 loppu = Vector3.zero;
    private float duration = 1;
    private Material material;

    private void OnEnable()
    {
        material = GetComponent<Renderer>().material;
    }

    public void Update()
    {
        if (cycleScale)
        {
            transform.localScale = Vector3.one * (baseScale + Mathf.Sin(Time.time * scaleSpeed) * scaleAmount);
        }
        else
        {
            transform.localScale = Vector3.one * baseScale;
        }
        if ((transform.position - loppu).magnitude > 0.003f)
        {
            transform.position = Vector3.Lerp(transform.position, loppu, speed * (Time.deltaTime / (1.0f / 60.0f)));
            if ((transform.position - loppu).magnitude < 0.003f)
            {
                transform.position = loppu;
                if (Valmis != null)
                    Valmis();
            }
        }

        if (korttiHavinnyt)
        {
            float t = (Time.time - korttiHavinnytAika) / duration;
            if (t > 1)
            {
                t = 1;
            }
            material.color = new Color(1, 1, 1, 1 - t);
        }
        else
        {
            material.color = new Color(1, 1, 1, 1);
        }
    }

    public void Mene(Vector2 kohde)
    {
        loppu = new Vector3(kohde.x, kohde.y);
    }

    public void Mene(CardSlot sijainti)
    {
        Mene(sijainti.transform.position);
    }

    [NonSerialized]
    public bool korttiHavinnyt = false;

    float korttiHavinnytAika = 999999.0f;

    public void SetToHide()
    {
        this.cycleScale = false;
        if (!korttiHavinnyt)
        {
            korttiHavinnyt = true;
            korttiHavinnytAika = Time.time;
        }
    }

    public void SetTarget(CardSlot target)
    {
        korttiHavinnyt = false;
        this.loppu = target.transform.position;
    }

    public void SetTarget(Vector3 target)
    {
        this.loppu = target;
    }

    public void SetAsVisible()
    {
        korttiHavinnyt = false;
    }
}
