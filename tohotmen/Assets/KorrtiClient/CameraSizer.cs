﻿using UnityEngine;

public class CameraSizer : MonoBehaviour
{
    void Start()
    {
        GetComponent<Camera>().orthographicSize = (float)(Screen.height) / Screen.width / 2;
    }
}
