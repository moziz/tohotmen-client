﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KorttiVisu : MonoBehaviour
{
    public Texture failsafe;
    public TextMesh text;
    public bool showText = true;

    public void Init(Card k)
    {
        KortinKuva kortinKuva = Kombot.ainio.korttienKuvat.Find(kortti =>
        {
            return kortti.kortti.ToString().ToLower() == k.type.ToString().ToLower();

        });
        GetComponent<MeshRenderer>().material.mainTexture = (kortinKuva != null) ? kortinKuva.kuva : failsafe;
        text.text = (showText && kortinKuva != null) ? kortinKuva.nimi : "";
    }
}
