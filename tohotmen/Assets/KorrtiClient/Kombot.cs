﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class KortinKuva
{
    public Tavara kortti;
    static int lol = 0;
    public Texture kuva { get { return kuvat[lol++ % kuvat.Length]; } }
    public Texture[] kuvat;
    public string nimi;
}

public class Data
{
    public static Kombot ainio = null;
}

public class Kombot : MonoBehaviour
{
    public List<KortinKuva> korttienKuvat;
    public List<Tehtava> tehtavat;
    
    public GameObject hilluvaKorttiPrefab;
    
    public static Kombot ainio = null;

    void Awake()
    {
        BlackBoard.Kombot = this;
        ainio = this;
        Data.ainio = this;

        tehtavat = null;
        if (tehtavat == null)
        {
            tehtavat = new List<Tehtava>();

            Tehtava t = new Tehtava();
            tehtavat.Add(t);
            t.rooli = Rooli.murhaaja;
            t.tarvii.Add(Tavara.ase);
            t.tarvii.Add(Tavara.radio);
            t.tarvii.Add(Tavara.valepuku);

            t = new Tehtava();
            tehtavat.Add(t);
            t.rooli = Rooli.kuski;
            t.tarvii.Add(Tavara.sanomalehti);
            t.tarvii.Add(Tavara.oljy);
            t.tarvii.Add(Tavara.valepuku);
            t.eiSaaOlla.Add(Tavara.viini);

        }
    }

}
