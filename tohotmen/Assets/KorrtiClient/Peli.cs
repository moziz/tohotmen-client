﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Tehtava
{
    public Rooli rooli;
    public List<Tavara> tarvii = new List<Tavara>();
    public List<Tavara> eiSaaOlla = new List<Tavara>();
}



[System.Serializable]
public enum Rooli
{
    murhaaja, kuski, muskelit, tarkkailija, syotti, aivot
}

[System.Serializable]
public enum Tavara
{
    ase, valepuku, oljy, radio, raha, viini, housut, sanomalehti, ruusukimppu, fail
}


public enum Sijainti
{
    KorttiYV, KorttiYO, KorttiAV, KorttiAO,
    KorttiVasenVaihtoaitio, KorttiOikeaVaihtoaitio,
    EiMikaan
}

public class Peli : MonoBehaviour
{
    public GameObject hilluvaKorttiPrefab;

    public Tehtava nykynenTehtava;
    
    public CardSlot paikkaYV;//{ get { return Vector3.right * (keskiPiste.x - korttiRuudunKoko.x * 0.5f) + Vector3.up * (keskiPiste.y - korttiRuudunKoko.y * 0.5f); } }
    public CardSlot paikkaYO;//{ get { return Vector3.right * (keskiPiste.x + korttiRuudunKoko.x * 0.5f) + Vector3.up * (keskiPiste.y - korttiRuudunKoko.y * 0.5f); } }
    public CardSlot paikkaAV;//{ get { return Vector3.right * (keskiPiste.x - korttiRuudunKoko.x * 0.5f) + Vector3.up * (keskiPiste.y + korttiRuudunKoko.y * 0.5f); } }
    public CardSlot paikkaAO;//{ get { return Vector3.right * (keskiPiste.x + korttiRuudunKoko.x * 0.5f) + Vector3.up * (keskiPiste.y + korttiRuudunKoko.y * 0.5f); } }
    public CardSlot paikkaViides; // TODO: ???
    public CardSlot paikkaVasenVaihtoaitio;//{ get { return paikkaVasen - Vector3.right * korttiRuudunKoko.x * 1.0f; } }
    public CardSlot paikkaOikeaVaihtoaitio;//{ get { return paikkaOikea + Vector3.right * korttiRuudunKoko.x * 1.0f; } }

    [NonSerialized] public List<CardSlot> handCardSlots;
    [NonSerialized] public List<CardSlot> allCardSlots;
    [NonSerialized] public Dictionary<Card, HilluvaKortti> kortit = new Dictionary<Card, HilluvaKortti>();

    void Awake()
    {
        this.allCardSlots = new List<CardSlot> { this.paikkaAO,this.paikkaAV, this.paikkaYO, this.paikkaYV, this.paikkaVasenVaihtoaitio, this.paikkaOikeaVaihtoaitio};
        this.handCardSlots = new List<CardSlot>() { this.paikkaAO, this.paikkaAV, this.paikkaYO, this.paikkaYV };
        BlackBoard.Peli = this;
    }

    public CardSlot Paikka(Sijainti sijainti)
    {
        return allCardSlots.Find((value) => value.sijainti == sijainti);
    }

    public HilluvaKortti CreateNewCard(Card k)
    {
        if (this.kortit.ContainsKey(k))
        {
            Debug.Log("Kortti on jo luotu: " + k);
            return this.kortit[k];
        }

        Debug.Log("Luodaan kortti: " + k);

        var kortti = GameObject.Instantiate(BlackBoard.Peli.hilluvaKorttiPrefab);
        kortti.transform.SetParent(BlackBoard.Peli.transform, false);

        var hilluvaKortti = kortti.GetComponent<HilluvaKortti>();
        this.kortit.Add(k, hilluvaKortti);
        kortti.GetComponent<KorttiVisu>().Init(k);
        return hilluvaKortti;
    }

    public CardSlot GetClosestSlot(Vector3 pos)
    {
        float closestDist = 99999999;
        int closestIndex = -1;
        for (int i = 0; i < BlackBoard.Peli.allCardSlots.Count; ++i)
        {
            Vector3 mov = pos - BlackBoard.Peli.allCardSlots[i].transform.position;
            if (mov.sqrMagnitude < closestDist)
            {
                closestDist = mov.sqrMagnitude;
                closestIndex = i;
            }
        }
        return BlackBoard.Peli.allCardSlots[closestIndex];
    }

    public void VaihdaKortit(CardSlot a, CardSlot b)
    {
        Card ac = a.currentCard;
        Card bc = b.currentCard;
        if (ac != null && ac != Card.EMPTY)
        {
            kortit[ac].SetTarget(b);
        }

        if (bc != null && bc != Card.EMPTY)
        {
            kortit[bc].SetTarget(a);
        }

        a.currentCard = bc;
        b.currentCard = ac;

        //send trade requests
        if (a == this.paikkaVasenVaihtoaitio || b == this.paikkaVasenVaihtoaitio)
        {
            BlackBoard.TitleScreenMenuManager.CurrentPlayerMakesOfferToLeft(this.paikkaVasenVaihtoaitio.currentCard);
        }

        if (a == this.paikkaOikeaVaihtoaitio || b == this.paikkaOikeaVaihtoaitio)
        {
            BlackBoard.TitleScreenMenuManager.CurrentPlayerMakesOfferToRight(this.paikkaOikeaVaihtoaitio.currentCard);
        }

    }
    

    static Tavara TavaraTekstista(string name)
    {
        if ( string.IsNullOrEmpty(name))
        {
            Debug.LogWarning("TavaraTekstista tyhja tai null");
            return Tavara.fail;
        }
        if (name == "ase") return Tavara.ase;
        if (name == "radio") return Tavara.radio;
        if (name == "valepuku") return Tavara.valepuku;
        if (name == "viini" || name == "alkoholi") return Tavara.viini;
        if (name == "housut") return Tavara.housut;
        if (name == "ruusukimppu" || name == "kukat" || name == "kukkia") return Tavara.ruusukimppu;
        if (name == "raha") return Tavara.raha;
        if (name == "oljy" || name == "öljy") return Tavara.oljy;
        if (name == "sanomalehti") return Tavara.sanomalehti;

        Debug.LogError("TavaraTekstista Failed: " + name);
        return Tavara.fail;
    }

    public void KorttiTulee(Card card, bool fromRight)
    {
        if (!this.kortit.ContainsKey(card))
        {
            CreateNewCard(card);
        }
        kortit[card].transform.position = new Vector3(fromRight ? 0.7f : -0.7f, -1, 1);
    }
    
    public void UpdateKorttiTilanne(Card[] kortitKadessa
        , Card korttiTarjousVasemmalle, Card korttiTarjousOikealle
        , Card korttiTarjousVasemmalta, Card korttiTarjousOikealta)
    {
        
        //Merkitse kaikki kortit piilotettavaksi
        //TODO tarkista jos kortti menee vaihtoon, niin transitioi ulos näytöstä, älä insta-piilota.
        foreach (var pari in this.kortit)
        {
            pari.Value.SetToHide();
        }

        //Käy käsikortit läpi
        //    - Kerro kortille tavoitteeksi olla kädessä
        for (int i = 0; i < kortitKadessa.Length; ++i)
        {
            if (this.kortit.ContainsKey(kortitKadessa[i]))
            {
                kortit[kortitKadessa[i]].SetAsVisible();
            }
        }

        //Remove old cards from hand
        for (int i = 0; i < this.handCardSlots.Count; ++i)
        {
            Card c = this.handCardSlots[i].currentCard;
            if (c != null && c != Card.EMPTY)
            {
                if (kortit[c].korttiHavinnyt)
                {
                    this.handCardSlots[i].currentCard = null;
                }
            }
        }

        //Tarkista että käsikorteilla on paikka
        for (int i = 0; i < kortitKadessa.Length; ++i)
        {
            if (this.kortit.ContainsKey(kortitKadessa[i]))
            {
                kortit[kortitKadessa[i]].SetAsVisible();
                // check it has a place in hand

                CardSlot slotInHand = this.handCardSlots.Find(slot => (slot.currentCard == kortitKadessa[i]));
                if (slotInHand == null)
                {
                    CardSlot freeSlot = GetFreeSlot();
                    if (freeSlot == null)
                    {
                        Debug.LogError("No freeslot for new card!");
                    }
                    else
                    {
                        this.kortit[kortitKadessa[i]].SetTarget(freeSlot);
                        freeSlot.currentCard = kortitKadessa[i];
                    }
                }
            }
            else
            {
                CreateNewCard(kortitKadessa[i]);
                //find free position
                CardSlot freeSlot = GetFreeSlot();
                if (freeSlot == null)
                {
                    Debug.LogError("No freeslot for new card!");
                }
                else
                {
                    this.kortit[ kortitKadessa[i]].SetTarget(freeSlot);
                    freeSlot.currentCard = kortitKadessa[i];
                }
            }
        }

        //Käy omat vaihtokortti läpi
        //    -kerro kortille että tavoite on että se on vaihdossa
        if (korttiTarjousVasemmalle != null && korttiTarjousVasemmalle.index != "")
        {
            if (!this.kortit.ContainsKey(korttiTarjousVasemmalle))
            {
                CreateNewCard(korttiTarjousVasemmalle);
                paikkaVasenVaihtoaitio.currentCard = korttiTarjousVasemmalle;
            }
            kortit[korttiTarjousVasemmalle].SetTarget(this.paikkaVasenVaihtoaitio);
        }
        else
        {
            paikkaVasenVaihtoaitio.currentCard = null;
        }

        if (korttiTarjousOikealle != null && korttiTarjousOikealle.index != "")
        {
            if (!this.kortit.ContainsKey(korttiTarjousOikealle))
            {
                CreateNewCard(korttiTarjousOikealle);
                paikkaOikeaVaihtoaitio.currentCard = korttiTarjousOikealle;
            }
            kortit[korttiTarjousOikealle].SetTarget(this.paikkaOikeaVaihtoaitio);
        }
        else
        {
            paikkaOikeaVaihtoaitio.currentCard = null;
        }

        //Käy tulevat kortit läpi
        //    -kerro niille että tavoite on olla tulossa X pelaajalta
        if (korttiTarjousVasemmalta != null && korttiTarjousVasemmalta != Card.EMPTY)
        {
            if (!this.kortit.ContainsKey(korttiTarjousVasemmalta))
            {
                CreateNewCard(korttiTarjousVasemmalta);
            }

            if (this.paikkaVasenVaihtoaitio.currentCard != korttiTarjousVasemmalta)
            {
                //first set
                kortit[korttiTarjousVasemmalta].transform.position = new Vector3(0.7f, -1, 1);
            }
            this.paikkaVasenVaihtoaitio.currentCard = korttiTarjousVasemmalta;
            kortit[korttiTarjousVasemmalta].SetTarget(this.paikkaVasenVaihtoaitio);
            kortit[korttiTarjousVasemmalta].cycleScale = true;
        }

        if (korttiTarjousOikealta != null && korttiTarjousOikealta != Card.EMPTY)
        {
            if (!this.kortit.ContainsKey(korttiTarjousOikealta))
            {
                CreateNewCard(korttiTarjousOikealta);
            }

            if (this.paikkaOikeaVaihtoaitio.currentCard != korttiTarjousOikealta)
            {
                //first set
                kortit[korttiTarjousOikealta].transform.position = new Vector3(-0.7f, -1, 1);
            }
            this.paikkaOikeaVaihtoaitio.currentCard = korttiTarjousOikealta;
            kortit[korttiTarjousOikealta].SetTarget(this.paikkaOikeaVaihtoaitio);
            kortit[korttiTarjousOikealta].cycleScale = true;
        }
        
        //päivitä hiiri
        if (BlackBoard.Hiiri.currentCard != null && BlackBoard.Hiiri.currentCard.index != "")
        {
            if (this.kortit[BlackBoard.Hiiri.currentCard].korttiHavinnyt)
            {
                BlackBoard.Hiiri.currentCard = null;
                BlackBoard.Hiiri.startCardSlot = null;
            }
        }
        BlackBoard.Hiiri.Update();
    }

    public CardSlot GetFreeSlot()
    {
        return this.handCardSlots.Find(slot => (slot.currentCard == null || slot.currentCard == Card.EMPTY));
    }

    public void Vaihe1Alkaa(Player current, Player playerOnLeft, Player playerOnRight)
    {
        foreach (var slot in allCardSlots)
        {
            slot.RemoveCard();
        }

        Card[] allCards = current.cardsInHand;
        string rooli = current.role;
        List<string> korttiSettiTavoite = new List<string>();
        List<string> korttiSettiNegatiivinenTavoite = new List<string>();

        //set tavoitteet
        foreach (string s in current.missionCards)
        {
            if (string.IsNullOrEmpty(s))
                continue;
            if (s[0] == '-')
                korttiSettiNegatiivinenTavoite.Add(s.TrimStart('-'));
            else
                korttiSettiTavoite.Add(s);
        }

        UpdateKorttiTilanne(current.cardsInHand, current.cardOfferLeft, current.cardOfferRight, playerOnLeft.cardOfferRight, playerOnRight.cardOfferLeft);
        nykynenTehtava = new Tehtava();
        nykynenTehtava.rooli = RooliTekstista(rooli);
        nykynenTehtava.tarvii = TavaratTekstista(korttiSettiTavoite);
        nykynenTehtava.eiSaaOlla = TavaratTekstista(korttiSettiNegatiivinenTavoite);
    }

    public List<Tavara> TavaratTekstista(List<string> tavarat)
    {
        List<Tavara> result = new List<Tavara>();
        foreach (string s in tavarat)
        {
            result.Add(TavaraTekstista(s));
        }
        return result;
    }
    public static Rooli RooliTekstista(string s)
    {
        if (s == "murhaaja") return Rooli.murhaaja;
        if (s == "kuski") return Rooli.kuski;
        if (s == "tarkkailija") return Rooli.tarkkailija;
        if (s == "muskelit") return Rooli.muskelit;
        if (s == "syötti" || s == "syotti") return Rooli.syotti;
        if (s == "aivot") return Rooli.aivot;
        return Rooli.murhaaja;
    }

}
