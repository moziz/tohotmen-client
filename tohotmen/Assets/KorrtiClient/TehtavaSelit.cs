﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class RooliTeksti
{
    public Rooli rooli;
    public string selite;
    public Sprite kuva;
    public Sprite miniKuva;
}

public class TehtavaSelit : MonoBehaviour
{
    public List<RooliTeksti> rooliTekstit = new List<RooliTeksti>();
    public PlayerIcon leftPlayerIcon;
    public PlayerIcon rightPlayerIcon;
    
    void Update()
    {
        RooliTeksti rooliTeksti = null;
        foreach(RooliTeksti t in rooliTekstit)
        {
            if(BlackBoard.Peli.nykynenTehtava.rooli == t.rooli)
            {
                rooliTeksti = t;
            }
        }

        if(rooliTeksti != null)
        {
            GetComponentInChildren<Text>().text = rooliTeksti.selite;
            transform.Find("RooliKuva").GetComponent<Image>().sprite = rooliTeksti.kuva;
        }

        Player leftPlayer = BlackBoard.TitleScreenMenuManager.GetLeft(BlackBoard.TitleScreenMenuManager.currentPlayer);
        Player rightPlayer = BlackBoard.TitleScreenMenuManager.GetRight(BlackBoard.TitleScreenMenuManager.currentPlayer);
        Rooli vasenRooli = Peli.RooliTekstista(leftPlayer.role);
        Rooli oikeaRooli = Peli.RooliTekstista(rightPlayer.role);
        
        foreach (RooliTeksti t in rooliTekstit)
        {
            if (t.rooli == vasenRooli)
            {
                leftPlayerIcon.Init(t.miniKuva, leftPlayer.name);
            }
            if (t.rooli == oikeaRooli)
            {
                rightPlayerIcon.Init(t.miniKuva, rightPlayer.name);
            }
        }
    }
}
