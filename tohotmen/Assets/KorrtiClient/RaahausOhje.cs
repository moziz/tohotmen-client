﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaahausOhje : MonoBehaviour
{

    float speed = 2.0f;
    
    void Start()
    {
        GetComponent<MeshRenderer>().material.color = new Color(1,1,1,0);
    }
    
    void Update()
    {
        var mat = GetComponent<MeshRenderer>().material;
        var c = mat.color;
        if (BlackBoard.Hiiri.currentCard != null)
        {
            c.a = Mathf.Min(c.a + Time.deltaTime * speed, 1.0f);
        }
        else
        {
            c.a = Mathf.Max(c.a - Time.deltaTime * speed, 0.0f);
        }
        mat.color = c;
    }
}
