﻿using System;
using UnityEngine;

public class Hiiri : MonoBehaviour
{
    [NonSerialized] public CardSlot startCardSlot = null;
    [NonSerialized] public Card currentCard = null;

    void Awake()
    {
        BlackBoard.Hiiri = this;
    }

    void AloitaRaahaus()
    {
        startCardSlot = BlackBoard.Peli.GetClosestSlot(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        currentCard = startCardSlot.currentCard;
        if (currentCard == Card.EMPTY)
        {
            currentCard = null;
        }
    }

    void LopetaRaahaus()
    {
        CardSlot raahauksenLoppu = BlackBoard.Peli.GetClosestSlot(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        Card temp = raahauksenLoppu.currentCard;
        BlackBoard.Peli.VaihdaKortit(startCardSlot, raahauksenLoppu);
        startCardSlot = null;
        currentCard = null;
    }

    public void Update()
    {
        float w = Screen.width;
        float h = Screen.height;

        if (Input.GetMouseButton(0) && startCardSlot == null)
        {
            AloitaRaahaus();
        }
        else if (startCardSlot != null && !Input.GetMouseButton(0))
        {
            LopetaRaahaus();
        }
        else if (currentCard != null)
        {
            BlackBoard.Peli.kortit[this.currentCard].SetTarget(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }

    }

}
