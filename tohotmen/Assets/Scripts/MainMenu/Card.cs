﻿
[System.Serializable]
public class Card {
    public static Card EMPTY
    {
        get{
            return new Card(){ index = "", type = "" };
        }
    }

    public string index;
    public string type;

    public override string ToString()
    {
        return type + "_" + index;
    }

    public override bool Equals(object obj)
    {
        if (obj is Card)
            return (obj as Card).index == index;
        return false;
    }

    public override int GetHashCode()
    {
        return int.Parse(index);
    }

    public static bool operator ==(Card a, Card b)
    {
        if (object.ReferenceEquals(a, b))
            return true;
        if (object.ReferenceEquals(a, null))
            return false;
        if (object.ReferenceEquals(b, null))
            return false;
        return a.index == b.index;
    }
    public static bool operator !=(Card a, Card b)
    {
        return !(a == b);
    }
}