﻿
public class PlayerMessage {
    public string status;
    public string type;
    public string eventData;

    public PlayerMessage(){
    }
    public PlayerMessage(string status, string type, string eventData) {
        this.status = status;
        this.type = type;
        this.eventData = eventData;
    }
}
