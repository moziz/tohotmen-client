﻿
public class GameMessage {
    public string status;
    public string type;
    public string eventData;
    public GameState game;

    public GameMessage(string status, string type, string eventData) {}
}

[System.Serializable]
public class TradeData
{
    public Player leftPlayer;
    public Player rightPlayer;
    public Card fromLeft;
    public Card fromRight;
}