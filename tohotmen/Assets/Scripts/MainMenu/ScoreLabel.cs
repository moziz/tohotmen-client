﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreLabel : MonoBehaviour {

    public Text scoreLabel; 
    // Use this for initialization
    void Start () {
        scoreLabel.text = "";
    }

    public void NewScoreInfo(int playerScore, int teamScore){
        scoreLabel.text = "OMAT PISTEET " + playerScore.ToString("### ###") + " MURHARYHMÄ " + teamScore.ToString("### ###");
    }
}
