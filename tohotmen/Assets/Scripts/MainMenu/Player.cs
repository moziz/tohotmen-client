﻿
[System.Serializable]
public class Player {
    public string index;
    public string name;
    public Card[] cardsInHand;
    public Card cardOfferLeft;
    public Card cardOfferRight;
    public string role;
    public string[] missionCards;
    public float score;
    public RoundScore[] roundScores;
}