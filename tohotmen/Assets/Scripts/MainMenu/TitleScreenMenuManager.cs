﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System;
using Random = UnityEngine.Random;

using WebSocketSharp;


public class TitleScreenMenuManager : MonoBehaviour
{
    public GameObject joinMenuGO;
    public GameObject waitMenuGO;
    public GameObject theGame;
    public LookTVMenu lookTVGO;
    public GameObject mainMenu;
    public GameObject alwaysOnCanvas;

    public GameObject waitListGO;
    public ScoreLabel score;
    public Text playerLabelGO;

    public List<Action<GameMessage>> onPlayerJoin = new List<Action<GameMessage>>();

    public InputField gameIndexInput;
    public InputField nameInput;
    public Text errorLabel;
    public WebSocket ws;

    public string host = "fgj18.tomikokkonen.fi";
    public string hostDebug = "localhost:29920";

    public string HOST
    {
        get
        {
#if UNITY_EDITOR
            return useDebugHost ? hostDebug : host;
#else
            return host;
#endif
        }
    }

    public bool useDebugHost = false;

    public string currentPlayerIndex;
    public GameState latestState;
    public GameState previousState;

    public Player currentPlayer
    {
        get
        {
            if (latestState == null || latestState.players == null)
                return null;
            for (int i = 0; i < this.latestState.players.Length; ++i)
            {
                if (this.latestState.players[i].index == this.currentPlayerIndex)
                {
                    return this.latestState.players[i];
                }
            }
            return null;
        }
    }

    public Player GetLeft(Player p)
    {
        int result = -1;
        if (this.latestState == null)
        {

            Debug.LogError("No last state");
            return null;
        }

        if (this.latestState.players == null)
        {
            Debug.LogError("last state has no players");
            return null;
        }

        for (int i = 0; i < this.latestState.players.Length; ++i)
        {
            if (this.latestState.players[i].index == p.index)
            {
                result = i - 1;
                break;
            }
        }
        if (result < 0)
        {
            result = this.latestState.players.Length - 1;
        }
        return this.latestState.players[result];
    }

    public Player GetRight(Player p)
    {
        int result = -1;
        for (int i = 0; i < this.latestState.players.Length; ++i)
        {
            if (this.latestState.players[i].index == p.index)
            {
                result = i + 1;
                break;
            }
        }
        if (result >= this.latestState.players.Length)
        {
            result = 0;
        }
        return this.latestState.players[result];
    }

    void Awake()
    {
        Application.targetFrameRate = 60;
        BlackBoard.TitleScreenMenuManager = this;
        this.alwaysOnCanvas.SetActive(true);
    }

    void Start()
    {
        errorLabel.text = "";
        theGame.SetActive(false);
        mainMenu.SetActive(true);
        lookTVGO.Close();
        joinMenuGO.SetActive(true);
        waitMenuGO.SetActive(false);
    }

    void Update()
    {
        if (ws != null && !string.IsNullOrEmpty(ws.error))
        {
            this.errorLabel.text = ws.error;
            joinMenuGO.SetActive(true);
            joinMenuGO.GetComponent<GoToLeft>().enabled = false;
            joinMenuGO.GetComponent<ComeFromRight>().OnEnable();
            waitMenuGO.SetActive(false);
            //ws.error = "";
            ws = null;
        }
        //Pura viesti jono
        if (this.ws != null && this.ws.IsConnected())
        {
            string message = ws.RecvString();
            while (message != null)
            {
                // Debug.Log("Got message from server" + message);
                HandleMessageFromServer(message);
                message = ws.RecvString();
            }
        }

    }

    public void HandleMessageFromServer(string message)
    {
        GameMessage gMessage;
        try
        {
            gMessage = JsonUtility.FromJson<GameMessage>(message);
        }
        catch (Exception e)
        {
            Debug.LogError("Failed to parse json from server " + message);
            return;
        }
        this.previousState = this.latestState;
        this.latestState = gMessage.game;
        if (previousState == null)
        {
            this.previousState = this.latestState;
        }
        Player current = currentPlayer;

        if (gMessage.type != "new-time")
        {
            print("Got message from server " + message);
        }

        switch (gMessage.type)
        {

            case "player-joined":
            case "player-removed":
                print("player-joined or removed message");

                foreach (var action in this.onPlayerJoin)
                {
                    if (action != null)
                    {
                        action.Invoke(gMessage);
                    }
                }

                break;

            case "you-joined":
                print("you-joined message");
                Player p = JsonUtility.FromJson<Player>(gMessage.eventData);
                this.currentPlayerIndex = p.index;
                waitMenuGO.SetActive(true);
                NamePlayer(nameInput.text);

                break;

            case "all-players-connected":
                print("you-joined connected");
                break;
            case "new-state":
                if (gMessage.eventData.EndsWith("-cutscene") || gMessage.eventData.EndsWith("-resolving"))
                {
                    theGame.SetActive(false);
                    mainMenu.SetActive(false);
                    lookTVGO.Open();
                }
                else if (gMessage.eventData.EndsWith("-trading"))
                {
                    theGame.SetActive(true);
                    mainMenu.SetActive(false);
                    lookTVGO.Close();
                    BlackBoard.Peli.Vaihe1Alkaa(current, GetLeft(current), GetRight(current));
                    BlackBoard.Peli.UpdateKorttiTilanne(current.cardsInHand,
                        current.cardOfferLeft,
                        current.cardOfferRight,
                        GetLeft(current).cardOfferRight,
                        GetRight(current).cardOfferLeft);
                }
                else
                {
                    Debug.LogWarning("What to do if state is: " + gMessage.eventData);
                }
                break;
            case "new-card-offer":
                //UpdateKorttiTilanne(string[] kortitKadessa, string korttiTarjousVasemmalle, string korttiTarjousOikealle, string korttiTarjousVasemmalta, string korttiTarjousOikealta)
                BlackBoard.Peli.UpdateKorttiTilanne(current.cardsInHand,
                    current.cardOfferLeft,
                    current.cardOfferRight,
                    GetLeft(current).cardOfferRight,
                    GetRight(current).cardOfferLeft);
                break;
            case "card-trade":
                TradeData tradeData = JsonUtility.FromJson<TradeData>(gMessage.eventData);
                if (tradeData.rightPlayer == currentPlayer)
                {
                    BlackBoard.Peli.KorttiTulee(tradeData.fromLeft, false);
                }
                else if (tradeData.leftPlayer == currentPlayer)
                {
                    BlackBoard.Peli.KorttiTulee(tradeData.fromRight, true);
                }
                break;
            case "new-score":
                this.score.NewScoreInfo(Mathf.CeilToInt(current.score), Mathf.CeilToInt(latestState.score));
                break;
            case "new-time":
                break;
            default:
                Debug.LogWarning("What to do if message is: " + gMessage.type);
                break;
        }
    }

    public void Join()
    {
        errorLabel.text = "";
        //joinMenuGO.SetActive(false);
        joinMenuGO.GetComponent<GoToLeft>().enabled = true;
        if (ws != null)
        {
            //peli jo käynnissä? pitäiskö tehdä jottain?
        }
        ws = new WebSocket(new System.Uri("ws://" + HOST + "/" + gameIndexInput.text));
        //ws = new WebSocket (new System.Uri("ws://localhost:8080"));
        //ws = new WebSocket ("ws://tomikokkonen.fi:8080/" + gameIndexInput);
        //ws = new WebSocket ("ws://echo.websocket.org");

        // ws.Connect ();
        StartCoroutine(ws.Connect());

        // print ("ReadyState: " + ws.ReadyState.ToString());
        // Foo ("pay-pay-payload");
    }

    public void AllPlayersJoined()
    {
        SendPlayerMessage(new PlayerMessage() { status = "1", type = "all-players-connected", eventData = "{}" });
    }

    public void NamePlayer(string name)
    {
        SendPlayerMessage(new PlayerMessage("1", "name-player", name));
    }

    public void Skip()
    {
        SendPlayerMessage(new PlayerMessage("ok", "player-skip", this.currentPlayerIndex));
    }

    public void SendPlayerMessage(PlayerMessage message)
    {
        string json = JsonUtility.ToJson(message);
        Debug.Log("Sending string to server: " + json);
        ws.SendString(json);
    }

    public void CurrentPlayerMakesOfferToRight(Card card)
    {
        SendPlayerMessage(new PlayerMessage("ok", "trade-offer-right", JsonUtility.ToJson(card)));
    }

    public void CurrentPlayerMakesOfferToLeft(Card card)
    {
        SendPlayerMessage(new PlayerMessage("ok", "trade-offer-left", JsonUtility.ToJson(card)));
    }
}
