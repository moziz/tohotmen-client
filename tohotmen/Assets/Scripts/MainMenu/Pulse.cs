﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulse : MonoBehaviour {

    public float deltaSize = 0.1f;
    public float originalSize = 0.9f;
    public float speed = 5;

    // Update is called once per frame
    void Update () {
        transform.localScale = Vector3.one * (originalSize + Mathf.Sin(Time.time * speed) * deltaSize);
    }
}
