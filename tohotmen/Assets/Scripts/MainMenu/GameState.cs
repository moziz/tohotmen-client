﻿
// waiting-players
// phase-1-trading
// phase-1-resolving
// phase-2-trading
// phase-2-resolving
// phase-3-trading
// phase-4-resolving
[System.Serializable]
public class GameState {
    public string state;
    public string joinCode;
    public Player[] players;
    public string[] skippingPlayers;
    public float score;
    public float round;
    public float timeLeft;
    public RoundScore[] roundScores;
}
