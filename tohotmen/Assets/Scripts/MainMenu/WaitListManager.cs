﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaitListManager : MonoBehaviour {

    public GameObject playerLabelGO;
    public TitleScreenMenuManager manager;

    private List<Text> labels = new List<Text>();

    void Start() {
        manager.onPlayerJoin.Add (this.ParsePlayersAndShow);
        ShowParticipants(manager.latestState.players);
    }

    void ShowParticipants(Player[] players) {
        Debug.Log("ShowParticipants" + players.Length);
        int i = 0;
        for (; i < players.Length; ++i) {
            while (labels.Count <= i) {
                var playerLabel = Instantiate(this.playerLabelGO);
                playerLabel.transform.SetParent(this.transform);
                playerLabel.transform.localScale = Vector3.one;
                this.labels.Add(playerLabel.GetComponent<Text>());
            }
            this.labels[i].text = players[i].name;
        }
        for (; i < labels.Count; ++i) {
            Destroy(labels[i]);
            labels.RemoveAt(i);
            --i;
        }
            
    }

    void ParsePlayersAndShow(GameMessage gameMessage) {
        ShowParticipants (gameMessage.game.players);
    }
}
