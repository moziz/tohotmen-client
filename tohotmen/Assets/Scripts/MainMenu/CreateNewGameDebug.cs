﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class CreateNewGameDebug : MonoBehaviour {
    public Button button;
    public Text label;
    public InputField username;
    public InputField gameId;
    public TitleScreenMenuManager menu;
    public void NewGame(){
        button.interactable = false;
        StartCoroutine(StartNewGameCoroutine());
    }

    private IEnumerator StartNewGameCoroutine(){

        UnityWebRequest req = new UnityWebRequest("http://" + menu.HOST + "/api/game/new-game/");
        req.downloadHandler = new DownloadHandlerBuffer();
        yield return req.SendWebRequest();
        Debug.Log("new game" + req.error);
        label.text = "KOODI: " + req.downloadHandler.text;
        username.text = "Riku";
        gameId.text = req.downloadHandler.text;
        menu.Join();
    }
}
