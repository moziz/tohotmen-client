﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameCodeLabel : MonoBehaviour {
    public Text label;
    
    // Use this for initialization
    void OnEnable () {
        if (BlackBoard.TitleScreenMenuManager != null) {
            if (BlackBoard.TitleScreenMenuManager.latestState != null) {
                this.label.text = "OTHERS CAN JOIN WITH THE CODE: " + BlackBoard.TitleScreenMenuManager.latestState.joinCode;
            }
        }
    }
}
