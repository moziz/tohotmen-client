﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardSlot : MonoBehaviour
{
    public Sijainti sijainti;
    public Card currentCard;

    public void RemoveCard()
    {
        this.currentCard = null;
    }
}
