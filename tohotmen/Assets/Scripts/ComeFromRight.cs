﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComeFromRight : MonoBehaviour {

    private float targetX;
    public float startX;
    public RectTransform rt;
    private float startTime;
    public float duration = 1;
    public AnimationCurve sCurve;

    // Use this for initialization
    public void OnEnable () {
        enabled = true;
        targetX = 0;// rt.anchoredPosition.x;
        if (rt.anchoredPosition.x >= -1) {
            rt.anchoredPosition = new Vector2(startX, rt.anchoredPosition.y);
        }
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update () {
        float t = (Time.time - startTime) / duration;
        if (t > 1) {
            t = 1;
            enabled = false;
        }
        t = sCurve.Evaluate(t);
        rt.anchoredPosition = new Vector2(startX * (1-t) + t * targetX, rt.anchoredPosition.y);
    }
}
