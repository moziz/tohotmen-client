﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LookTVMenu : MonoBehaviour
{
    public GameObject skipButton;
    public GameObject skipRequestedLabel;

    // Use this for initialization
    public void Open () {
        gameObject.SetActive(true);
        skipButton.SetActive(true);
        skipRequestedLabel.SetActive(false);
    }

    // Update is called once per frame
    public void SkipButton () {
        skipButton.SetActive(false);
        skipRequestedLabel.SetActive(true);
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }
}
