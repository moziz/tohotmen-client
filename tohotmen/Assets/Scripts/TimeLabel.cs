﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeLabel : MonoBehaviour {

    public Text label;
    public int lastValue = -1338;

    void Start(){
        label.text = "";
    }
    // Update is called once per frame
    void Update () {
        if (BlackBoard.TitleScreenMenuManager != null && BlackBoard.TitleScreenMenuManager.latestState != null) {
            int currentTime = Mathf.RoundToInt(BlackBoard.TitleScreenMenuManager.latestState.timeLeft);
            if (currentTime != lastValue) {
                lastValue = currentTime;
                if (currentTime <= 0) {
                    label.text = "";
                } else {
                    int minutes = currentTime / 60;
                    int seconds = currentTime % 60;
                    label.text = "" + minutes + ":" + ((seconds < 10) ? "0" : "") + seconds;
                }
            }
        }
    }
}
