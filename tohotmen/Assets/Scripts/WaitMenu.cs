﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaitMenu : MonoBehaviour
{

    public Button startButton;
    public GameObject minimumLabel;

    // Use this for initialization
    void Start()
    {
        BlackBoard.TitleScreenMenuManager.onPlayerJoin.Add(this.OnPlayerJoin);
        bool canStart = BlackBoard.TitleScreenMenuManager.latestState != null &&
                        BlackBoard.TitleScreenMenuManager.latestState.players.Length > 1;
        startButton.interactable = canStart;
        minimumLabel.SetActive(!canStart);
    }

    // Update is called once per frame
    void OnPlayerJoin(GameMessage gm)
    {
        bool canStart = gm.game != null && gm.game.players.Length > 1;
        startButton.interactable = canStart;
        minimumLabel.SetActive(!canStart);
    }
}
