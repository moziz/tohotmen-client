﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BlackBoard {
    public static TitleScreenMenuManager TitleScreenMenuManager;
    public static Peli Peli;
    public static ScoreLabel ScoreLabel;
    public static Kombot Kombot;
    public static Hiiri Hiiri;
}
